FROM node:latest as builder
WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .
RUN npm run build

FROM nginx:latest

COPY --from=builder /app/certs/server.crt /etc/ssl/server.crt
COPY --from=builder /app/certs/server.key /etc/ssl/server.key
COPY --from=builder /app/dist/ng-rev-expnd-budget-bg-trading-app /usr/share/nginx/html
COPY --from=builder /app/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
EXPOSE 443
