import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { gql } from '@apollo/client';
import { ProductClass } from './domains/product-class';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductClassService {
  constructor(private apollo: Apollo) {}

  GET_PRODUCT_CLASS = gql`
    query productClass {
      productClass {
        id
        name
      }
    }
  `;

  async findAll(): Promise<ProductClass[]> {
    const result = await lastValueFrom(
      this.apollo.query<any>({ query: this.GET_PRODUCT_CLASS })
    );

    return result.data['productClass'] as ProductClass[];
  }
}
