import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatPaginatorIntl } from '@angular/material/paginator';

@Injectable({
  providedIn: 'root',
})
export class PaginatorIntlService extends MatPaginatorIntl {
  translate?: TranslateService;

  override getRangeLabel = (page: number, pageSize: number, length: number) => {
    const of =
      this.translate !== undefined
        ? this.translate?.instant('paginator.of')
        : 'of';
    if (length == 0 || pageSize == 0) {
      return `0 ${of} ${length}`;
    }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex =
      startIndex < length
        ? Math.min(startIndex + pageSize, length)
        : startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} ${of} ${length}`;
  };

  translateLabels() {
    this.itemsPerPageLabel = this.translate?.instant('paginator.itemsPerPage');
    this.nextPageLabel = this.translate?.instant('paginator.nextPage');
    this.previousPageLabel = this.translate?.instant('paginator.previousPage');
  }

  constructor(translate: TranslateService) {
    super();
    this.translate = translate;

    translate.onLangChange.subscribe(() => {
      this.translateLabels();
    });

    this.translateLabels();
  }
}
