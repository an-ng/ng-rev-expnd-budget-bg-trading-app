import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentTableDialogComponent } from './document-table-dialog.component';

describe('DocumentTableDialogComponent', () => {
  let component: DocumentTableDialogComponent;
  let fixture: ComponentFixture<DocumentTableDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentTableDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentTableDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
