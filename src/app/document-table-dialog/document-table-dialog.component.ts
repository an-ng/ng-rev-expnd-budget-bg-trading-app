import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { Document } from '../domains/document';
import { CompanyService } from '../company.service';
import { DocumentDetailDialogComponent } from '../document-detail-dialog/document-detail-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-document-table-dialog',
  templateUrl: './document-table-dialog.component.html',
  styleUrls: ['./document-table-dialog.component.scss'],
})
export class DocumentTableDialogComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort?: MatSort;
  @ViewChild(MatPaginator) paginator?: MatPaginator;
  loading = false;

  displayedColumns = [
    'date',
    'documentType',
    'companySenderCode',
    'companySenderName',
    'companyRecipientCode',
    'companyRecipientName',
    'total',
    'currency',
    'actions',
  ];

  constructor(
    private companyService: CompanyService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DocumentTableDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { documents: Document[] },
    public translate: TranslateService
  ) {}

  async ngOnInit() {
    this.loading = true;
    const docs = this.data.documents;
    const companyCodes = Array.from(
      new Set([
        ...docs.map((d) => d.companySenderCode),
        ...docs.map((d) => d.companyMiddlemanCode),
        ...docs.map((d) => d.companyRecipientCode),
      ])
    );

    const companies = await this.companyService.findAllByCodes(companyCodes);
    this.dataSource.data = [
      ...docs.map((d: Document) => {
        let document = new Document(d);
        document.date0 = moment(d.date0).format('DD.MM.yyyy');
        if (!!d.companySenderCode) {
          const companySender = companies.find(
            (c) => c.id === d.companySenderCode
          );
          if (companySender)
            document.companySenderName = companySender.name
              ? companySender.name.trim()
              : '';
        }
        if (!!d.companyMiddlemanCode) {
          const companyMiddleman = companies.find(
            (c) => c.id === d.companyMiddlemanCode
          );
          if (companyMiddleman)
            document.companyMiddlemanName = companyMiddleman.name
              ? companyMiddleman.name.trim()
              : '';
        }
        if (!!d.companyRecipientCode) {
          const companyRecipient = companies.find(
            (c) => c.id === d.companyRecipientCode
          );

          if (companyRecipient)
            document.companyRecipientName = companyRecipient.name
              ? companyRecipient.name.trim()
              : '';
        }
        return document;
      }),
    ];
    if (this.sort) this.dataSource.sort = this.sort;
    if (this.paginator) this.dataSource.paginator = this.paginator;
    // console.log(typeof (this.dataSource.data as any[])[0].date0);
    this.loading = false;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  viewDocument(element: Document) {
    console.log(element);
    this.dialog.open(DocumentDetailDialogComponent, {
      width: '75vw',
      data: {
        document: element,
      },
    });
  }

  closeDialog(e: any) {
    this.dialogRef.close();
  }
}
