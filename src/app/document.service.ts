import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { OperationPair } from './domains/operation-pair';
import { gql } from '@apollo/client';
import { lastValueFrom } from 'rxjs';
import { AccountTransaction } from './domains/account-transaction';
import { Document } from './domains/document';

@Injectable({
  providedIn: 'root',
})
export class DocumentService {
  constructor(private apollo: Apollo) {}

  GET_DOCUMENT_BY_CODES = gql`
    query documentByCodes($codes: [Int!]!) {
      documentByCodes(codes: $codes) {
        id
        date0
        documentType
        documentIdentifier
        companySenderCode
        companyMiddlemanCode
        companyRecipientCode
        comment
        total
        currency
        documentItems {
          id
          productCode
          productSeriesCode
          operationGroupCode
          unit
          quantity0
          currency
          price
          total
        }
      }
    }
  `;

  async getDocumentsByCodes(codes: number[]) {
    const queryResult = await lastValueFrom(
      this.apollo.query<any>({
        query: this.GET_DOCUMENT_BY_CODES,
        variables: {
          codes,
        },
      })
    );

    return queryResult.data['documentByCodes'] as Document[];
  }

  async getAccountTransactions(request: any) {
    const queryResult = await lastValueFrom(
      this.apollo.query<any>({
        query: gql`
          query accTx($req: AccTxReq) {
            accTx(req: $req) {
              id
              documentCode
              date
              documentType
              operationGroupCode
              companySenderCode
              companyRecipientCode
              productCode
              productSeriesCode
              total
              currency
              productClassCode
            }
          }
        `,
        variables: {
          req: request,
        },
      })
    );
    return queryResult.data['accTx'] as AccountTransaction[];
  }

  async getRevenueAccTxList() {
    const now = new Date();
    const request = {
      startDate: new Date(2017, 1, 1),
      endDate: new Date(now.getFullYear() + 1, 11),
      operationPairList: [new OperationPair(undefined, 46)],
    };
    return this.getAccountTransactions(request);
  }

  async getExpenditureAccTxList() {
    const now = new Date();
    const request = {
      startDate: new Date(2017, 1, 1),
      endDate: new Date(now.getFullYear() + 1, 11),
      operationPairList: [new OperationPair(20, undefined)],
    };
    return this.getAccountTransactions(request);
  }
}
