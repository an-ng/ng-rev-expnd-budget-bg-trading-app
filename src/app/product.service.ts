import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { gql } from '@apollo/client';
import { Product } from './domains/product';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private apollo: Apollo) {}

  GET_PRODUCT = gql`
    query product {
      product {
        id
        name
      }
    }
  `;

  GET_PRODUCT_BY_CODES = gql`
    query productByCodes($codes: [Int!]!) {
      productByCodes(codes: $codes) {
        id
        name
      }
    }
  `;

  async findAll(): Promise<Product[]> {
    const result = await lastValueFrom(
      this.apollo.query<any>({ query: this.GET_PRODUCT })
    );

    return result.data['product'] as Product[];
  }

  async findAllByCodes(codes: number[]): Promise<Product[]> {
    const result = await lastValueFrom(
      this.apollo.query<any>({
        query: this.GET_PRODUCT_BY_CODES,
        variables: { codes },
      })
    );

    return result.data['productByCodes'] as Product[];
  }
}
