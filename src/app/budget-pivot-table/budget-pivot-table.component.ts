import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../document.service';
import { ProductService } from '../product.service';
import { ProductClassService } from '../product-class.service';
import { CompanyService } from '../company.service';
import { AccountTransaction } from '../domains/account-transaction';
import { Product } from '../domains/product';
import { ProductClass } from '../domains/product-class';
import { Company } from '../domains/company';
import { loadMessages, locale } from 'devextreme/localization';
import bgMessages from './bg.json';
import { MatDialog } from '@angular/material/dialog';
import { DocumentTableDialogComponent } from '../document-table-dialog/document-table-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import ruMessages from 'devextreme/localization/messages/ru.json';
import enMessages from 'devextreme/localization/messages/en.json';
@Component({
  selector: 'app-budget-pivot-table',
  templateUrl: './budget-pivot-table.component.html',
  styleUrls: ['./budget-pivot-table.component.scss'],
})
export class BudgetPivotTableComponent implements OnInit {
  loading = false;
  constructor(
    private documentService: DocumentService,
    private companyService: CompanyService,
    private productService: ProductService,
    private productClassService: ProductClassService,
    public translate: TranslateService,
    public dialog: MatDialog
  ) {
    translate.onLangChange.subscribe(() => {
      this.changeDxComponentLang();
    });
    this.changeDxComponentLang();
  }

  changeDxComponentLang() {
    const lang = localStorage.getItem('app_lang');
    switch (lang) {
      case 'bg':
        loadMessages(bgMessages);
        break;
      case 'en':
        loadMessages(enMessages);
        break;
      case 'ru':
        loadMessages(ruMessages);
        break;
      default:
        loadMessages(enMessages);
        break;
    }
    this.initDataSourceFields();
    locale(navigator.language);
  }

  initDataSourceFields() {
    while (this.pivotGridDataSource.fields.length > 0) {
      this.pivotGridDataSource.fields.pop();
    }
    const newFields = [
      {
        area: 'data',
        dataField: 'total',
        dataType: 'number',
        summaryType: 'sum',
        caption: this.translate.instant('total'),

        format: {
          type: 'currency',
          currency: 'BGN',
        },
      },
      {
        caption: this.translate.instant('date'),
        area: 'column',
        dataField: 'date',
        dataType: 'date',
      },
      {
        caption: this.translate.instant('type'),
        area: 'row',
        dataField: 'accountName',
        width: 120,
      },
      {
        caption: this.translate.instant('group'),
        area: 'row',
        dataField: 'companyGroupCodeName',
        width: 160,
      },
      {
        caption: this.translate.instant('subject'),
        area: 'row',
        dataField: 'companyRecipientCodeName',
        width: 160,
      },
      {
        caption: this.translate.instant('productClass'),
        area: 'row',
        dataField: 'productClassName',
        width: 140,
      },
      {
        caption: this.translate.instant('productName'),
        area: 'row',
        dataField: 'productCodeName',
        width: 140,
      },
    ];
    for (const field of newFields) {
      this.pivotGridDataSource.fields.push(field);
    }
    console.log(this.pivotGridDataSource.fields);
  }

  pivotGridDataSource: any = {
    fields: [
      {
        area: 'data',
        dataField: 'total',
        dataType: 'number',
        summaryType: 'sum',
        caption: 'Сума',

        format: {
          type: 'currency',
          currency: 'BGN',
        },
      },
      {
        caption: 'Дата',
        area: 'column',
        dataField: 'date',
        dataType: 'date',
      },
      {
        caption: 'Тип',
        area: 'row',
        dataField: 'accountName',
        width: 120,
      },
      {
        caption: 'Група',
        area: 'row',
        dataField: 'companyGroupCodeName',
        width: 160,
      },
      {
        caption: 'Корр.-т',
        area: 'row',
        dataField: 'companyRecipientCodeName',
        width: 160,
      },
      {
        caption: 'Клас.-ия',
        area: 'row',
        dataField: 'productClassName',
        width: 140,
      },
      {
        caption: 'Име',
        area: 'row',
        dataField: 'productCodeName',
        width: 140,
      },
    ],
  };

  private createTx(
    t: AccountTransaction,
    products: Product[],
    productClasses: ProductClass[],
    companies: Company[]
  ): any {
    const tx: any = { ...t };
    if (!!t.productCode) {
      const found = products.find((p) => p.id === t.productCode);
      if (found) tx.productCodeName = `${found.id}: ${found.name}`;
    }

    if (!!t.productClassCode) {
      const found = productClasses.find((pc) => pc.id === t.productClassCode);
      if (found) tx.productClassName = found.name;
    }
    if (!!t.companySenderCode) {
      const found = companies.find((c) => c.id === t.companySenderCode);
      if (found) tx.companySenderCodeName = `${found.id}: ${found.name}`;
    }
    if (!!t.companyRecipientCode) {
      const found = companies.find((c) => c.id === t.companyRecipientCode);
      if (found) {
        tx.companyRecipientCodeName = `${found.id}: ${found.name}`;
        if (found.parentCode) {
          const companyGroup = companies.find((c) => c.id === found.parentCode);
          if (companyGroup)
            tx.companyGroupCodeName = `${companyGroup.id}: ${companyGroup.name}`;
        }
      }
    }
    return tx;
  }

  async ngOnInit() {
    this.loading = true;
    const rev = await this.documentService.getRevenueAccTxList();
    const expnd = await this.documentService.getExpenditureAccTxList();
    const productCodes = new Set([
      ...expnd.map((t) => t.productCode),
      ...rev.map((t) => t.productCode),
    ]);
    const products = await this.productService.findAllByCodes(
      Array.from(productCodes)
    );
    const productClasses = await this.productClassService.findAll();
    const companies = await this.companyService.findAll();
    let transactionList: any[] = [];
    rev.forEach((t) => {
      const tx = this.createTx(t, products, productClasses, companies);
      tx.accountName = 'Доходи (46 сметка)';
      transactionList = [...transactionList, tx];
    });
    expnd.forEach((t) => {
      const tx = this.createTx(t, products, productClasses, companies);
      tx.accountName = 'Разходи (20 сметка)';
      transactionList = [...transactionList, tx];
    });
    this.pivotGridDataSource = {
      ...this.pivotGridDataSource,
      store: [...transactionList],
    };
    this.loading = false;
  }

  onPivotCellClick(e: any) {
    if (this.loading) {
      return;
    }
    if (e.area === 'data') {
      this.loading = true;
      const pivotGridDataSource = e.component.getDataSource();
      const drillDownDataSource = pivotGridDataSource.createDrillDownDataSource(
        e.cell
      );
      drillDownDataSource
        .store()
        .load()
        .then((res: AccountTransaction[]) => {
          const codes = Array.from(new Set(res.map((t) => t.documentCode)));
          this.documentService.getDocumentsByCodes(codes).then((docs) => {
            this.dialog.open(DocumentTableDialogComponent, {
              id: 'document-table-dialog',
              width: '90vw',
              data: { documents: docs },
            });
            this.loading = false;
          });
        });
    }
  }
}
