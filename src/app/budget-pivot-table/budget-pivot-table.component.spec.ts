import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetPivotTableComponent } from './budget-pivot-table.component';

describe('BudgetPivotTableComponent', () => {
  let component: BudgetPivotTableComponent;
  let fixture: ComponentFixture<BudgetPivotTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BudgetPivotTableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetPivotTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
