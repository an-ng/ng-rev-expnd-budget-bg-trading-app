import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Document } from '../domains/document';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ProductService } from '../product.service';
import { DocumentItem } from '../domains/document-item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-document-detail-dialog',
  templateUrl: './document-detail-dialog.component.html',
  styleUrls: ['./document-detail-dialog.component.scss'],
})
export class DocumentDetailDialogComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort?: MatSort;
  @ViewChild(MatPaginator) paginator?: MatPaginator;
  displayedColumns = [
    'id',
    'productCode',
    'productName',
    'quantity0',
    'unit',
    'price',
    'total',
    'currency',
  ];
  document?: Document;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { document: Document },
    public productService: ProductService,
    public translate: TranslateService
  ) {}

  private async documentItemPreprocess(items: DocumentItem[]) {
    const products = await this.productService.findAllByCodes(
      Array.from(new Set(items.map((di) => di.productCode)))
    );
    return [
      ...items.map((i) => {
        let item = new DocumentItem(i);
        if (i.productCode !== 0) {
          const found = products.find((p) => p.id === i.productCode);
          if (found) item.productName = found.name;
        }
        return item;
      }),
    ];
  }

  async ngOnInit() {
    this.document = this.data.document;
    if (this.document.documentItems !== undefined) {
      const items = await this.documentItemPreprocess(
        this.document.documentItems
      );
      console.log(items);
      this.dataSource.data = items;
      if (this.sort) this.dataSource.sort = this.sort;
      if (this.paginator) this.dataSource.paginator = this.paginator;
    }
  }
}
