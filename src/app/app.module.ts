import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { InMemoryCache } from '@apollo/client';
import {
  HttpClient,
  HttpClientModule,
  HttpHeaders,
} from '@angular/common/http';
import { HttpLink } from 'apollo-angular/http';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { BudgetPivotTableComponent } from './budget-pivot-table/budget-pivot-table.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {
  DxButtonModule,
  DxDataGridModule,
  DxPivotGridModule,
} from 'devextreme-angular';
import { FlexModule } from '@angular/flex-layout';
import { DocumentTableDialogComponent } from './document-table-dialog/document-table-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { DocumentDetailDialogComponent } from './document-detail-dialog/document-detail-dialog.component';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PaginatorIntlService } from './paginator-intl.service';
import { MatMenuModule } from '@angular/material/menu';
import {
  AuthConfig,
  OAuthModule,
  OAuthModuleConfig,
} from 'angular-oauth2-oidc';
import { HomeComponent } from './home/home.component';
import { MatCardModule } from '@angular/material/card';

export const authConfig: AuthConfig = {
  issuer: environment.issuer,
  redirectUri: window.location.origin + '/',
  clientId: environment.clientId,
  showDebugInformation: true,
  scope: 'openid profile',
  tokenEndpoint: `${environment.issuer}/protocol/openid-connect/token`,
  userinfoEndpoint: `${environment.issuer}/protocol/openid-connect/userinfo`,
  logoutUrl: `${environment.issuer}/protocol/openid-connect/logout?client_id=${
    environment.clientId
  }&returnTo=${encodeURIComponent(window.location.origin)}`,
  responseType: 'code',
};

const authModuleConfig: OAuthModuleConfig = {
  resourceServer: {
    allowedUrls: [environment.backendUri],
    sendAccessToken: true,
  },
};

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function createApollo(httpLink: HttpLink) {
  const token = localStorage.getItem('token');

  const cache = new InMemoryCache();
  const link = httpLink.create({
    uri: environment.backendUri,
    headers: new HttpHeaders({
      Authorization: `Bearer ${token}`,
    }),
  });

  return {
    link,
    cache,
  };
}

@NgModule({
  declarations: [
    AppComponent,
    BudgetPivotTableComponent,
    DocumentTableDialogComponent,
    DocumentDetailDialogComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    OAuthModule.forRoot(authModuleConfig),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatProgressBarModule,
    DxPivotGridModule,
    FlexModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    DxDataGridModule,
    DxButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatTabsModule,
    MatPaginatorModule,
    TranslateModule.forRoot({
      defaultLanguage: 'bg',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    MatMenuModule,
    MatCardModule,
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
    {
      provide: MatPaginatorIntl,
      useFactory: (translate: TranslateService) => {
        return new PaginatorIntlService(translate);
      },
      deps: [TranslateService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
