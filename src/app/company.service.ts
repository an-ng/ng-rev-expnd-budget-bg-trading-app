import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { gql } from '@apollo/client';
import { Company } from './domains/company';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  constructor(private apollo: Apollo) {}

  GET_COMPANY_BY_CODES = gql`
    query companyByCodes($codes: [Int!]!) {
      companyByCodes(codes: $codes) {
        id
        parentCode
        name
      }
    }
  `;

  GET_COMPANY = gql`
    query company {
      company {
        id
        name
        parentCode
      }
    }
  `;

  async findAll(): Promise<Company[]> {
    const result = await lastValueFrom(
      this.apollo.query<any>({ query: this.GET_COMPANY })
    );
    return result.data['company'] as Company[];
  }

  async findAllByCodes(codes: (number | undefined)[]) {
    const result = await lastValueFrom(
      this.apollo.query<any>({
        query: this.GET_COMPANY_BY_CODES,
        variables: { codes },
      })
    );
    return result.data['companyByCodes'] as Company[];
  }
}
