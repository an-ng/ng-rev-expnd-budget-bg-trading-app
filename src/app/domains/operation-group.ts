import { Operation } from './operation';

export interface OperationGroup {
  id: number;
  parentCode: number;
  name: string;
  operations?: Operation[];
}
