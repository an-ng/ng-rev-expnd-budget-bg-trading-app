import { ProductClass } from './product-class';

export interface Product {
  id?: number;
  name?: string;
  productClass?: ProductClass;
}
