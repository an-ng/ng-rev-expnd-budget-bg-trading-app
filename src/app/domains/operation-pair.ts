export class OperationPair {
  accountDebitCode?: number;
  accountCreditCode?: number;

  constructor(accountDebitCode?: number, accountCreditCode?: number) {
    this.accountDebitCode = accountDebitCode;
    this.accountCreditCode = accountCreditCode;
  }
}
