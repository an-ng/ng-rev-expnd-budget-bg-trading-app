import { OperationGroup } from './operation-group';

export interface Operation {
  id: number;
  operationGroup?: OperationGroup;
  name: string;
  type?: number;
  accountDebitCode?: string;
  accountCreditCode?: string;
  amountType: number;
  companyDebit: number;
  companyCredit: number;
}
