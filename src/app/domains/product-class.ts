import { Product } from './product';

export interface ProductClass {
  id?: number;
  name?: string;
  parentCode?: number;
  products?: Product[];
}
