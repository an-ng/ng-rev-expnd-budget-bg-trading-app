export interface Company {
  id: number;
  name?: string;
  parentCode?: number;
  itn?: string;
  okonh?: string;
  okpo?: string;
}
