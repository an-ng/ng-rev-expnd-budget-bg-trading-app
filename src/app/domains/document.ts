import { DocumentItem } from './document-item';

function getCodeName(codeValue: number, nameValue: string) {
  if (codeValue === 0) return '';
  return `${codeValue}: ${nameValue}`;
}

export class Document {
  constructor(d: Document) {
    this.id = d.id;
    this.date0 = d.date0;
    this.date1 = d.date1;
    this.time = d.time;
    this.documentType = d.documentType;
    this.documentIdentifier = d.documentIdentifier;
    this.companyRecipientCode = d.companyRecipientCode;
    this.companyMiddlemanCode = d.companyMiddlemanCode;
    this.companySenderCode = d.companySenderCode;
    this.total = d.total;
    this.currency = d.currency;
    this.documentItems = d.documentItems;
  }

  id: number = 0;
  date0?: string;
  date1?: string;
  time: number = 0;
  documentType?: string;
  documentIdentifier?: string;
  companyRecipientCode: number = 0;
  companyRecipientName: string = '';
  companyMiddlemanCode: number = 0;
  companyMiddlemanName: string = '';
  companySenderCode: number = 0;
  companySenderName: string = '';
  comment?: string;
  total: number = 0;
  currency?: string;
  documentItems?: DocumentItem[];

  get companySenderCodeName() {
    return getCodeName(this.companySenderCode, this.companySenderName);
  }

  get companyMiddlemanCodeName() {
    return getCodeName(this.companyMiddlemanCode, this.companyMiddlemanName);
  }

  get companyRecipientCodeName() {
    return getCodeName(this.companyRecipientCode, this.companyRecipientName);
  }

  get getComment() {
    return this.comment ? this.comment : '';
  }
}
