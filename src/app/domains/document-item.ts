import { OperationGroup } from './operation-group';

export class DocumentItem {
  constructor(i: DocumentItem) {
    this.id = i.id;
    this.document = i.document;
    this.operationGroupCode = i.operationGroupCode;
    this.operationGroup = i.operationGroup;
    this.productCode = i.productCode;
    this.productName = i.productName;
    this.productSeriesCode = i.productSeriesCode;
    this.unit = i.unit;
    this.quantity0 = i.quantity0;
    this.quantity1 = i.quantity1;
    this.quantity2 = i.quantity2;
    this.price = i.price;
    this.currency = i.currency;
    this.total = i.total;
  }

  id: number = 0;
  document?: Document;
  operationGroupCode: number = 0;
  operationGroup?: OperationGroup;
  productCode: number = 0;
  productName?: string;
  productSeriesCode: number = 0;
  unit: string = '';
  quantity0: number = 0;
  quantity1: number = 0;
  quantity2: number = 0;
  price: number = 0;
  currency: string = '';
  total: number = 0;
}
