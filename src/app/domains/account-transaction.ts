export interface AccountTransaction {
  id?: number;
  documentCode: number;
  documentType?: string;
  date?: Date;
  date1?: Date;
  operationGroupCode?: number;
  companySenderCode?: number;
  companyMiddlemanCode?: number;
  companyRecipientCode?: number;
  productCode: number;
  productSeriesCode?: number;
  price?: number;
  quantity0?: number;
  total?: number;
  currency?: string;
  comment?: string;
  productClassCode: number;
  operationGroup: any;
  // For pivot table
  productCodeName?: string;
  productClassName?: string;
  companyRecipientCodeName?: string;
}
