import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { OAuthErrorEvent, OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from './app.module';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ng-rev-expnd-budget-bg-trading-app';
  opened = false;
  languages = new Map([
    ['bg', 'Български'],
    ['en', 'English'],
    ['ru', 'Русский'],
  ]);

  links = [{ path: 'budget', label: 'БДР' }];

  constructor(
    @Inject(DOCUMENT) public document: Document,
    public oauthService: OAuthService,
    public translate: TranslateService
  ) {
    const language = localStorage.getItem('app_lang');
    translate.setDefaultLang('bg');
    if (!language) {
      translate.use('bg');
      localStorage.setItem('app_lang', 'bg');
    } else {
      translate.use(language);
    }
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();

    // For debugging:
    oauthService.events.subscribe((e) =>
      e instanceof OAuthErrorEvent ? console.error(e) : console.warn(e)
    );
    oauthService.setupAutomaticSilentRefresh();

    oauthService.loadDiscoveryDocumentAndLogin();
  }

  useLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem('app_lang', language);
    window.location.reload();
  }

  getUsername(): string {
    const claims: any = this.oauthService.getIdentityClaims();
    if (!claims) return '';
    return claims['preferred_username'] ?? '';
  }
}
