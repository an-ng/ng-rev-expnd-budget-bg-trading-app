export const environment = {
  production: true,
  issuer: 'https://10.10.1.6:9700/realms/dn-serv',
  clientId: 'rev-expnd-budget-bg-app',
  backendUri: 'https://10.10.1.6:9050/graphql',
};
